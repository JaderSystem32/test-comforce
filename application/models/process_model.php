<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Process_model extends CI_Model
{
    /**
     * This function is used to get the process listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
     function processListingCount($searchText = '')
     {
         $this->db->select('*');
         $this->db->from('tbl_processes as BaseTbl');
         $this->db->join('tbl_places as Place', 'Place.placeId = BaseTbl.placeId','inner');
         if(!empty($searchText)) {
             $likeCriteria = "(BaseTbl.date  ='".$searchText."')";
             $this->db->where($likeCriteria);
         }

         $query = $this->db->get();

         return count($query->result());
     }

    /**
     * This function is used to get the process listing
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
     function processListing($searchText = '', $page, $segment)
     {
         $this->db->select('*');
         $this->db->from('tbl_processes as BaseTbl');
         $this->db->join('tbl_places as Place', 'Place.placeId = BaseTbl.placeId','inner');
         if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.date ='".$searchText."')";
            $this->db->where($likeCriteria);
         }
         $this->db->limit($page, $segment);
         $query = $this->db->get();

         $result = $query->result();
         return $result;
     }

    /**
     * This function is used to add new process to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewProcess($processInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_processes', $processInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

    /**
     * This function used to get process information by id
     * @param number $processId : This is process id
     * @return array $result : This is process information
     */
    function getProcessInfo($processId)
    {
        $this->db->select('*');
        $this->db->from('tbl_processes as BaseTbl');
        $this->db->join('tbl_places as Place', 'Place.placeId = BaseTbl.placeId','inner');
        $this->db->where('BaseTbl.processId', $processId);
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to update the process information
     * @param array $processInfo : This is processes updated information
     * @param number $processId : This is process id
     */
    function editProcess($processInfo, $processId)
    {
        $this->db->where('processId', $processId);
        $this->db->update('tbl_processes', $processInfo);

        return TRUE;
    }

    /**
     * This function is used to delete the process information
     * @param number $processId : This is process id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteProcess($processId)
    {
        $this->db->delete('tbl_processes', array('processId' => $processId));

        return $this->db->affected_rows();
    }

    /**
     * This function is used to check whether process number id is already exist or not
     * @param {string} $consecutive : This is process number
     * @param {number} $processId : This is process id
     * @return {mixed} $result : This is searched result
     */
    function checkConsecutiveExists($consecutive, $processId = 0)
    {
        $this->db->select("consecutive");
        $this->db->from("tbl_processes");
        $this->db->where("consecutive", $consecutive);
        if($processId != 0){
            $this->db->where("processId !=", $processId);
        }
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function is used to get process consecutive
     * @return {mixed} $consecutive : This is consecutive
     */

    function getConsecutive()
    {
      $this->db->select("consecutive");
      $this->db->from("tbl_processes");
      $this->db->order_by('processId', 'DESC');
      $this->db->limit(1);

      $result = $this->db->get()->result();

      if($result != null){
          $consecutive = (int) $result[0]->consecutive;
          $consecutive += 1;
          $consecutive = str_pad($consecutive, (9 - count($consecutive)), "0", STR_PAD_LEFT);
      }else{
          $consecutive = '00000001';
      }

      return $consecutive;

    }
}
