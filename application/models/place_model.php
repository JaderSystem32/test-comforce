<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Place_model extends CI_Model
{
    /**
     * This function is used to get the place listing
     * @return array $result : This is result
     */
     function placeListing()
     {
         $this->db->select('*');
         $this->db->from('tbl_places');

         $query = $this->db->get();

         $result = $query->result();
         return $result;
     }
}
