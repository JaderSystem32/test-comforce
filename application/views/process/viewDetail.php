<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-male"></i> Process Management
        <small>Add / Edit / View Process</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url(); ?>processListing"><i class="fa fa-male"></i> Processes</a></li>
        <li class="active">View Detail Process</li>
      </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">View Detail Process</h3>
                    </div><!-- /.box-header -->
                    <table  class="table table-bordered table-hover">
                        <?php foreach ($processInfo as $key => $value): ?>
                        <tr>
                            <th>Process Number</th>
                            <td><?php echo $value->consecutive; ?></td>
                        </tr>
                        <tr>
                            <th>Place</th>
                            <td><?php echo $value->name; ?></td>
                        </tr>
                        <tr>
                            <th>Creation Date</th>
                            <td><?php echo $value->date; ?></td>
                        </tr>
                        <tr>
                            <th>Budget</th>
                            <td><?php echo $value->budget; ?></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><?php echo $value->description; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
