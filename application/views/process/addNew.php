<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-male"></i> Process Management
        <small>Add / Edit / View Process</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url(); ?>processListing"><i class="fa fa-male"></i> Processes</a></li>
        <li class="active">Add Process</li>
      </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Process Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="addProcess" action="<?php echo base_url() ?>addNewProcess" method="post" role="form">
                        <div class="box-body">
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <?php echo form_label('Budget', 'budget'); ?><span class="process-required">*</span>
                                    <input type="number" name="budget" id="budget" class="form-control required" >
                                    <!-- /.input group -->
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <?php echo form_label('Place', 'placeId'); ?><span class="process-required">*</span>
                                      <?php echo form_dropdown('placeId', $places, '', 'class="form-control required"'); ?>
                                  </div>
                              </div>
                          </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <?php echo form_label('Creation Date', 'datepicker-date'); ?><span class="process-required">*</span>
                                    <div class="input-group date">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" name="date"  readonly value="<?php echo date('Y-m-d'); ?>"id="datepicker-date" class="form-control pull-right required" >
                                    </div>
                                    <!-- /.input group -->
                                  </div>
                                </div>
                                <div class="col-md-6">

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo form_label('Description', 'description'); ?><span class="process-required">*</span>
                                        <?php echo form_textarea(array('name'=>'description','id'=>'description','maxlength'=>'200','class'=>'form-control required','placeholder'=>'Description')); ?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-warning" value="Submit" />
                            <input type="reset" id="btn-reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/addProcess.js" type="text/javascript"></script>
<script type="text/javascript">

$( document ).ready(function() {

  $('#btn-reset').on('click', function(){
    $('#rate-field').hide();
  });
});
</script>
