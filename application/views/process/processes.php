<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-male"></i> Process Management
        <small>Add, Edit</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Process</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-warning" href="<?php echo base_url(); ?>addNewProcessForm"><i class="fa fa-plus"></i> Add Process</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Processes List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>processListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText"  value="<?php echo $searchText; ?>"id="datepicker-dateSearch" class="form-control pull-right" style="width:200px" data-date-format="yyyy-mm-dd" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <table id="table-processes" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Process Number <i class="fa fa-sort" aria-hidden="true"></i></th>
                        <th>Description <i class="fa fa-sort" aria-hidden="true"></i></th>
                        <th>Creation Date <i class="fa fa-sort" aria-hidden="true"></i></th>
                        <th>Place <i class="fa fa-sort" aria-hidden="true"></i></th>
                        <th>Budget <i class="fa fa-sort" aria-hidden="true"></i></th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($processRecords))
                        {
                            foreach($processRecords as $record)
                            {
                        ?>
                        <tr>
                          <td><?php echo $record->consecutive ?></td>
                          <td><?php echo $record->description ?></td>
                          <td><?php echo $record->date ?></td>
                          <td><?php echo $record->name ?></td>
                          <td><?php echo $record->budget ?></td>
                          <td class="text-center">
                              <a class="btn btn-sm btn-success" href="<?php echo base_url().'viewDetailProcess/'.$record->processId; ?>"><i class="fa fa-search"></i></a>
                              <a class="btn btn-sm btn-info" href="<?php echo base_url().'editOldProcess/'.$record->processId; ?>"><i class="fa fa-pencil"></i></a>
                              <!--<a class="btn btn-sm btn-danger deleteProcess" href="#" data-processid="<?php echo $record->processId; ?>"><i class="fa fa-trash"></i></a>-->
                          </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Process Number</th>
                      <th>Description </th>
                      <th>Creation Date </th>
                      <th>Place </th>
                      <th>Budget </th>
                      <th class="text-center">Actions</th>
                    </tr>
                    </tfoot>
                  </table>

                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "processListing/" + value);
            jQuery("#searchList").submit();
        });

        $('#datepicker-dateSearch').datepicker({
            autoclose: true,
            endDate: "today"
        });

        $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});

        $("[data-mask]").inputmask();
    });

    $(function () {
      $('#table-processes').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": true
      });
    });
</script>
