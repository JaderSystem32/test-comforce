<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Tablero
        <small>Panel de Control</small>
      </h1>
    </section>

    <section class="content">
        <div class="row">
          <?php
          if($role == ROLE_ADMIN)
          {
          ?>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>Users</h3>
                  <p>Admin</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <a href="<?php echo base_url(); ?>userListing" class="small-box-footer">Go to <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          <?php } ?>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box" style="background-color: #1e282c">
                <div class="inner">
                  <h3 style="color:#fff">Processes</h3>
                  <p style="color:#fff">All</p>
                </div>
                <div class="icon">
                  <i class="fa fa-male" style="color:white;"></i>
                </div>
                <a href="<?php echo base_url(); ?>processListing" class="small-box-footer">Go to<i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
    </section>
</div>
