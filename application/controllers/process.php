<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Process (ProcessController)
 * Process Class to control all process related operations.
 * @author : Jader Yepez
 * @version : 1.0
 * @since : October 2018
 */
class Process extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->global['dashboard'] = '';
        $this->global['users'] = '';
        $this->global['processes'] = 'active';
        $this->load->model('process_model');
        $this->load->model('place_model');
        if($this->session->userdata('isLoggedIn')){ $this->isLoggedIn(); }
    }

    /**
     * This function is used to load the process list
     */
    function processListing()
    {
        $this->load->model('process_model');

        $searchText = $this->input->post('searchText');
        $data['searchText'] = $searchText;

        $this->load->library('pagination');

        $count = $this->process_model->processListingCount($searchText);

			  $returns = $this->paginationCompress ( "processListing/", $count, 10 );

        $data['processRecords'] = $this->process_model->processListing($searchText, $returns["page"], $returns["segment"]);

        $this->global['pageTitle'] = 'Comforce-test : Process Listing';

        $this->loadViews("process/processes", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        $this->load->model('process_model');

        $this->global['pageTitle'] = 'Comforce-test : Add New Process';

        $places = array();

        foreach ($this->place_model->placeListing() as $value){
          $places += array(
                        $value->placeId => $value->name
                      );

        }

        $data['places'] = $places;

        $this->loadViews("process/addNew", $this->global, $data, NULL);
    }

    /**
     * This function is used to add new process to the system
     */
    function addNewProcess()
    {
        $this->load->library('form_validation');

        //$this->form_validation->set_rules('consecutive','Process Number','trim|required|max_length[8]|xss_clean');
        $this->form_validation->set_rules('description','Description','trim|required|xss_clean|max_length[200]');
        $this->form_validation->set_rules('date','Creation Date','required|xss_clean');
        $this->form_validation->set_rules('placeId','Place','required|xss_clean');
        $this->form_validation->set_rules('budget','Budget','required|numeric|xss_clean');

        if($this->form_validation->run() == FALSE)
        {
            $this->addNew();
        }
        else
        {
            $consecutive = $this->process_model->getConsecutive();          
            $description = $this->input->post('description');
            $date = $this->input->post('date');
            $budget = $this->input->post('budget');
            $placeId = $this->input->post('placeId');
            $userId = 1;

            $processInfo = array('consecutive'=>$consecutive, 'description'=>$description, 'date'=>$date, 'budget'=> $budget,'placeId'=> $placeId,
                                'userId'=>$userId);

            $result = $this->process_model->addNewProcess($processInfo);

            if($result > 0)
            {
                $this->session->set_flashdata('success', 'New Process created successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Process creation failed');
            }

            redirect('addNewProcessForm');
        }

    }


    /**
     * This function is used load process edit information
     * @param number $processId : Optional : This is process id
     */
    function editOld($processId = NULL)
    {
        if($processId == null)
        {
            redirect('processListing');
        }

        $data['processInfo'] = $this->process_model->getProcessInfo($processId);

        $places = array();

        foreach ($this->place_model->placeListing() as $value){
          $places += array(
                        $value->placeId => $value->name
                      );

        }

        $data['places'] = $places;

        $this->global['pageTitle'] = 'Comforce-test : Edit Process';

        $this->loadViews("process/editOld", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the process information
     */
    function editProcess()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('consecutive','Process Number','trim|required|max_length[8]|xss_clean');
        $this->form_validation->set_rules('description','Description','trim|required|xss_clean|max_length[200]');
        $this->form_validation->set_rules('date','Creation Date','required|xss_clean');
        $this->form_validation->set_rules('placeId','Place','required|xss_clean');
        $this->form_validation->set_rules('budget','Budget','required|numeric|xss_clean');

        $processId = $this->input->post('processId');

        if($this->form_validation->run() == FALSE)
        {
            $this->editOld($processId);
        }
        else
        {
            $consecutive = $this->input->post('consecutive');
            $description = $this->input->post('description');
            $date = $this->input->post('date');
            $budget = $this->input->post('budget');
            $placeId = $this->input->post('placeId');
            $userId = 1;

            $processInfo = array('consecutive'=>$consecutive, 'description'=>$description, 'budget'=>$budget,'date'=>$date, 'placeId'=> $placeId,
                                'userId'=>$userId);

            $resultProcess = $this->process_model->editProcess($processInfo, $processId);

            if($resultProcess == true)
            {
                $this->session->set_flashdata('success', 'Process updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Process updation failed');
            }

            redirect('processListing');
        }

    }

    /**
     * This function is used view process information
     * @param number $processId : This is process id
     */
    function viewDetail($processId)
    {
        $data['processInfo'] = $this->process_model->getProcessInfo($processId);

        $this->global['pageTitle'] = 'Comforce-test : View Detail Process';

        $this->loadViews("process/viewDetail", $this->global, $data, NULL);
    }

    /**
     * This function is used to delete the process using processId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteProcess()
    {
        $processId = $this->input->post('processId');

        $result = $this->process_model->deleteProcess($processId, $processInfo);

        if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
        else { echo(json_encode(array('status'=>FALSE))); }

    }

    /**
    * This function is used to check whether process number already exist or not
    */
   function checkConsecutiveExistsProcess()
   {
       $processId = $this->input->post("processId");
       $consecutive = $this->input->post("consecutive");

       if(empty($processId)){
           $result = $this->process_model->checkConsecutiveExists($consecutive);
       } else {
           $result = $this->process_model->checkConsecutiveExists($consecutive, $processId);
       }

       if(empty($result)){ echo("true"); }
       else { echo("false"); }
   }


    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Comforce-test : 404 - Page Not Found';

        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>
