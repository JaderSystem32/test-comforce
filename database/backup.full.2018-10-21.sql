-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-10-2018 a las 02:18:38
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `comforce_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_places`
--

CREATE TABLE `tbl_places` (
  `placeId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_places`
--

INSERT INTO `tbl_places` (`placeId`, `name`) VALUES
(1, 'Bogotá'),
(2, 'México'),
(3, 'Perú');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_processes`
--

CREATE TABLE `tbl_processes` (
  `processId` int(11) NOT NULL,
  `date` date NOT NULL,
  `consecutive` tinytext NOT NULL,
  `description` text NOT NULL,
  `budget` int(10) NOT NULL,
  `userId` int(11) NOT NULL,
  `placeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_processes`
--

INSERT INTO `tbl_processes` (`processId`, `date`, `consecutive`, `description`, `budget`, `userId`, `placeId`) VALUES
(1, '2018-10-20', '00000001', 'descriptiom gggggggggggggggggggggggggggggggggggg', 3000000, 1, 3),
(2, '2018-10-21', '00000002', 'Description 2', 1500000, 1, 1),
(3, '2018-10-22', '00000003', 'dfffffffffffffffffffffffffff', 200000, 1, 3),
(4, '2018-10-22', '00000004', 'dddddddddddddhhhhhhhhhhhhhh', 44444, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` int(11) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'System Administrator'),
(3, 'User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_sessions`
--

CREATE TABLE `tbl_sessions` (
  `session_id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `user_agent` varchar(100) NOT NULL DEFAULT '0',
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_data` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_sessions`
--

INSERT INTO `tbl_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('9a6a1e23b78748e1eef2353501977b4b', '0.0.0.0', 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0', '0000-00-00 00:00:00', 'a:1:{s:15:"flash:new:error";s:26:"Email or pa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  `roleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`, `roleId`) VALUES
(1, 'admin@admin.com', '$2y$10$WIvnQHw7o5IRicoDkhBaPefGE4miqC99jdE.sjMl47X03SqYjJ.ya', 'System Administrator', '9890098900', 0, 0, '2015-07-01 18:56:49', 1, '2017-06-09 22:05:47', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_places`
--
ALTER TABLE `tbl_places`
  ADD PRIMARY KEY (`placeId`);

--
-- Indices de la tabla `tbl_processes`
--
ALTER TABLE `tbl_processes`
  ADD PRIMARY KEY (`processId`),
  ADD KEY `foreing_k_userId_idx` (`userId`),
  ADD KEY `foreing_k_placeId_idx` (`placeId`);

--
-- Indices de la tabla `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- Indices de la tabla `tbl_sessions`
--
ALTER TABLE `tbl_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `ci_sessions_timestamp` (`user_agent`);

--
-- Indices de la tabla `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`),
  ADD KEY `forein_k_roleId_idx` (`roleId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_places`
--
ALTER TABLE `tbl_places`
  MODIFY `placeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_processes`
--
ALTER TABLE `tbl_processes`
  MODIFY `processId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_processes`
--
ALTER TABLE `tbl_processes`
  ADD CONSTRAINT `foreing_k_placeId` FOREIGN KEY (`placeId`) REFERENCES `tbl_places` (`placeId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreing_k_userId` FOREIGN KEY (`userId`) REFERENCES `tbl_users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
