/**
 * File : addProcess.js
 *
 * This file contain the validation of add process form
 *
 * Using validation plugin : jquery.validate.js
 *
 * @author Jader Yepez
 */

$(document).ready(function(){

	var addProcessForm = $("#addProcess");

	var validator = addProcessForm.validate({

		rules:{
      consecutive : { required : true, remote : { url : baseURL + "checkConsecutiveExistsProcess", type :"post" } },
			description :{ required : true },
			date : { required : true, date : true},
			placeId : { required : true},
			budget : { required : true, digits : true },
		},
		messages:{
      consecutive : { required : "This field is required", remote : "Process Number already taken" },
			description :{ required : "This field is required" },
			date : { required : "This field is required", date: "Please enter valid date"},
			placeId :{ required : "This field is required" },
			budget : { required : "This field is required", digits : "Please enter numbers only. Ex: 45.999" },
		}
	});
});
