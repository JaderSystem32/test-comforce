/**
 * File : editProcess.js
 *
 * This file contain the validation of edit form
 *
 * Using validation plugin : jquery.validate.js
 *
 * @author Jader Yepez
 */

$(document).ready(function(){

	var editProcessForm = $("#editProcess");

	var validator = editProcessForm.validate({

		rules:{
      consecutive : { required : true, remote : { url : baseURL + "checkConsecutiveExistsProcess", type :"post", data : { processId : function(){ return $("#processId").val(); } } } },
			description :{ required : true },
			date : { required : true, date : true},
			placeId : { required : true},
			budget : { required : true, decimal : true },
		},
		messages:{
			consecutive : { required : "This field is required", remote : "Process Number already taken" },
			description :{ required : "This field is required" },
			date : { required : "This field is required", date: "Please enter valid date"},
			placeId :{ required : "This field is required" },
			budget : { required : "This field is required", digits : "Please enter numbers only. Ex: 45.999" },
		}
	});
});
